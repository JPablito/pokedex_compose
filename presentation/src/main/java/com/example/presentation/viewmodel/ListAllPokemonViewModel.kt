package com.example.presentation.viewmodel

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex_compose.domain.model.Pokemon
import com.example.pokedex_compose.domain.user_case.get_pokemon.DetailPokemonUseCase
import com.example.pokedex_compose.domain.user_case.get_pokemons.ListAllPokemonUseCase
import kotlinx.coroutines.launch

class ListAllPokemonViewModel(
    private val getListAllPokemonUseCase: ListAllPokemonUseCase,
    private val getDetailPokemonUseCase: DetailPokemonUseCase
) : ViewModel() {

    val pokemon = mutableStateOf<List<Pokemon>>(emptyList())
    val isLoading = mutableStateOf(false)

    fun listAllPokemon() {

        viewModelScope.launch {
            isLoading.value = true
            val pokemonList: ArrayList<com.example.pokedex_compose.domain.model.Pokemon> = ArrayList()
            getListAllPokemonUseCase.execute(151).results.let { pokemonAllList ->

                pokemonAllList.forEach { pokemonDetail ->
                    getDetailPokemonUseCase.execute(pokemonDetail.name).let { pokemonList.add(it) }
                }

                pokemon.value = pokemonList
            }
            isLoading.value = false
        }

    }

}