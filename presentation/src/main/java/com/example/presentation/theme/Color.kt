package com.example.presentation.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val BackgroundColor = Color(0xFF1E1E1E)
val onAppBarColor = Color(0xFF2A2A2A)
val PurpleItem = Color(0xFF302F51)
val ColorAbility = Color(0xFFFF5757)
val AnimationColor = Color(0xFF52FF00)