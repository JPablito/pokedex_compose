package com.example.presentation.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.pokedex_compose.domain.model.Pokemon
import com.example.presentation.component.details_pokemon.DetailsStatsView
import com.example.presentation.component.details_pokemon.ItemAbilities
import com.example.presentation.component.details_pokemon.ItemSprites
import com.example.presentation.component.TitleText
import com.example.presentation.theme.onAppBarColor


@Composable
fun DetailsScreen(pokemon: Pokemon, navController: NavController) {

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = pokemon.name, color = Color.White)
                }, navigationIcon = {
                    IconButton(onClick = {

                        navController.popBackStack()

                    }) {
                        Icon(
                            Icons.Filled.ArrowBack,
                            contentDescription = "Back button",
                            tint = Color.White
                        )
                    }
                },
                backgroundColor = onAppBarColor
            )
        },
        modifier = Modifier.fillMaxSize(),
        backgroundColor = onAppBarColor
    ) {

        DetailScreenView(pokemon = pokemon)

    }

}

@Composable
fun DetailScreenView(pokemon: Pokemon) {
    
    Column(
        modifier = Modifier
            .fillMaxHeight()
    ) {

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(170.dp)
        ) {

            AsyncImage(
                model = pokemon.sprites.front_default,
                contentDescription = "",
                modifier = Modifier
                    .align(Alignment.Center)
                    .size(150.dp)
                    .padding(top = 24.dp)
            )

        }

        TitleText(titleText = "Abilities")

        LazyRow(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 8.dp, vertical = 8.dp)
        ) {

            items(pokemon.abilities){ pokemon ->
                ItemAbilities(textAbility = pokemon.ability.name)

            }

        }

        TitleText(titleText = "Stats")

        Spacer(modifier = Modifier.height(6.dp))

        for (i in 0..4){
            DetailsStatsView(
                statName = pokemon.stats[i].stat.name,
                baseStatValue = pokemon.stats[i].base_stat.toString()
            )
        }

        TitleText(titleText = "Sprites", paddingTop = 12.dp)

        LazyRow {

            val listSprites = listOf(
                pokemon.sprites.front_default,
                pokemon.sprites.front_female,
                pokemon.sprites.front_shiny,
                pokemon.sprites.front_shiny_female
            )

            items(listSprites) { pokemonSprite ->
                ItemSprites(imageText = pokemonSprite)
            }
        }
    }
}


