package com.example.presentation.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavController
import com.example.presentation.component.all_pokemon.ListAllItemPokemon
import com.example.presentation.theme.BackgroundColor
import com.example.presentation.theme.ColorAbility
import com.example.presentation.theme.onAppBarColor
import com.example.presentation.viewmodel.ListAllPokemonViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun AllPokemonScreen(
    viewModel: ListAllPokemonViewModel = koinViewModel(),
    navController: NavController
) {

    val pokemons by viewModel.pokemon
    val isLoading by viewModel.isLoading

    LaunchedEffect(true) {
        if (pokemons.isEmpty()) viewModel.listAllPokemon()
    }

    Scaffold(topBar = {
        TopAppBar(
            title = { Text(text = "Pokedex", color = Color.White) },
            backgroundColor = onAppBarColor
        )}, backgroundColor = BackgroundColor)
    {

        Box(
            modifier = Modifier
                .fillMaxSize(),
        ) {

            if (!isLoading) ListAllItemPokemon(pokemons, navController)
            else CircularProgressIndicator(
                modifier = Modifier.align(Alignment.Center),
                color = ColorAbility
            )

        }
    }

}