package com.example.presentation.component.details_pokemon

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.presentation.theme.AnimationColor

@Composable
fun DetailsStatsView(
    statName: String,
    baseStatValue: String,
    topPadding: Dp = 6.dp
) {

    Row(
        modifier = Modifier
            .padding(topPadding)
            .fillMaxWidth()
    ) {

        Text(
            text = statName,
            color = Color.White,
            modifier = Modifier
                .width(140.dp),
            textAlign = TextAlign.End,
        )

        BoxWithConstraints(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .clip(RoundedCornerShape(22.dp))
                .height(20.dp)
                .background(Color.LightGray),
        ) {

            val width = this.maxWidth
            val maxValue = 255
            val currentValue = baseStatValue.toInt()
            val percentage = currentValue.toFloat() / maxValue.toFloat()

            var animationPlayed by remember {
                mutableStateOf(false)
            }

            LaunchedEffect(key1 = true){
                animationPlayed = true
            }

            val currentPercent = animateFloatAsState(
                targetValue = if (animationPlayed) percentage else 0f,
                animationSpec = tween(
                    1000,
                    0
                )
            )
            val totalWidth = width * currentPercent.value

            Box(
                modifier = Modifier
                    .width(totalWidth)
                    .fillMaxHeight()
                    .clip(RoundedCornerShape(22.dp))
                    .background(AnimationColor),
            )

            Text(
                text = baseStatValue,
                modifier = Modifier
                    .padding(start = 8.dp)
                    .align(Alignment.CenterStart)
            )
        }
    }
}