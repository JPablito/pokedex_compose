package com.example.presentation.component

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun TitleText(
    modifier: Modifier = Modifier,
    titleText: String,
    paddingStart: Dp = 16.dp,
    paddingTop: Dp = 6.dp
) {

    Text(
        text = titleText,
        modifier = modifier.padding(
            start = paddingStart,
            top = paddingTop
        ),
        color = Color.White,
        fontSize = 18.sp
    )

}