package com.example.presentation.component.all_pokemon

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.example.pokedex_compose.domain.model.Pokemon.Companion.toJson

@Composable
fun ListAllItemPokemon(
    pokemonList: List<com.example.pokedex_compose.domain.model.Pokemon>,
    navController: NavController
) {

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
    ) {

        items(items = pokemonList){ pokemonItem ->
            ItemPokemon(
                pokemon = pokemonItem,
                onClick = {
                    navController.navigate("details_pokemon/${pokemonItem.toJson()}")
                })
        }
    }
}