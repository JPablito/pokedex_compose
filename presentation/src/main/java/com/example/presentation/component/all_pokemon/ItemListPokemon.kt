package com.example.presentation.component.all_pokemon

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.pokedex_compose.domain.model.Pokemon
import com.example.presentation.theme.BackgroundColor
import com.example.presentation.theme.PurpleItem
import com.example.presentation.theme.onAppBarColor

@Composable
fun ItemPokemon(
    pokemon: Pokemon,
    onClick: (Pokemon) -> Unit
) {

    Box(modifier = Modifier
        .fillMaxWidth()
        .background(color = BackgroundColor)
        .padding(top = 16.dp, start = 12.dp)
        .clickable {
            onClick.invoke(pokemon)
        }) {

        Surface(
            shape = RoundedCornerShape(16.dp),
            elevation = 1.dp,
            modifier = Modifier
                .fillMaxWidth()
                .height(70.dp)
                .padding(start = 60.dp, end = 30.dp)
                .align(alignment = Alignment.Center),
            color = onAppBarColor

        ) {

            Text(
                text = pokemon.name,
                style = MaterialTheme.typography.subtitle2,
                color = Color.White,
                textAlign = TextAlign.Center,
                fontSize = 20.sp,
                modifier = Modifier.padding(top = 20.dp)
            )

        }

        AsyncImage(
            model = pokemon.sprites.front_default, contentDescription = "img",
            modifier = Modifier
                .align(alignment = Alignment.CenterStart)
                .clip(CircleShape)
                .size(110.dp)
                .background(PurpleItem)
        )

    }

}