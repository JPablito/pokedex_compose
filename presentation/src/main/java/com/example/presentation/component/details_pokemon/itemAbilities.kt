package com.example.presentation.component.details_pokemon

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.presentation.theme.ColorAbility

@Composable
fun ItemAbilities(textAbility: String) {

    androidx.compose.material.Surface(
        shape = RoundedCornerShape(16.dp),
        color = ColorAbility,
        modifier = Modifier.padding(horizontal = 4.dp)
    ) {
        Text(
            text = textAbility,
            color = Color.White,
            modifier = Modifier
                .padding(horizontal = 12.dp, vertical = 4.dp)
        )
    }
}