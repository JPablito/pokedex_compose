package com.example.presentation.component.details_pokemon

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage

@Composable
fun ItemSprites(imageText: String?) {

    Box(modifier = Modifier.padding(start = 8.dp)) {

        imageText?.let {
            AsyncImage(
                model = imageText, contentDescription = "Pokemon sprite",
                modifier = Modifier
                    .size(120.dp)
            )
        }

    }

}
