package com.example.presentation.navigation

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.pokedex_compose.domain.model.Pokemon
import com.example.presentation.ui.DetailsScreen
import com.example.presentation.ui.SplashScreen
import com.example.presentation.ui.AllPokemonScreen
import com.google.gson.Gson

abstract class JsonNavType<T> : NavType<T>(isNullableAllowed = false) {
    abstract fun fromJsonParse(value: String): T
    abstract fun T.getJsonParse(): String

    override fun get(bundle: Bundle, key: String): T? =
        bundle.getString(key)?.let { parseValue(it) }

    override fun parseValue(value: String): T = fromJsonParse(value)

    override fun put(bundle: Bundle, key: String, value: T) {
        bundle.putString(key, value.getJsonParse())
    }
}

class PokemonArgType : JsonNavType<com.example.pokedex_compose.domain.model.Pokemon>() {
    override fun fromJsonParse(value: String): com.example.pokedex_compose.domain.model.Pokemon = Gson().fromJson(value, com.example.pokedex_compose.domain.model.Pokemon::class.java)
    override fun com.example.pokedex_compose.domain.model.Pokemon.getJsonParse(): String = Gson().toJson(this)

    //override fun FromPokemonArguments.getJsonParse(): String = Gson().toJson(this)
}

@Composable
fun NavigationGraph() {

    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = NavigationRoutes.SplashScreenRoute.route
    ) {

        composable(NavigationRoutes.SplashScreenRoute.route){ SplashScreen(navController = navController) }

        composable(NavigationRoutes.ListPokemonScreenRoute.route) { AllPokemonScreen(navController = navController) }

        composable(route = "details_pokemon/{pokemon}", arguments = listOf(

            navArgument("pokemon"){
                //type = NavType.ParcelableType(Pokemon::class.java)
                //type = NavType.StringType
                type = PokemonArgType()
            })

        ) {

            val pokemonJson = remember{
                it.arguments?.getString("pokemon")
            }

//            val pokemonJson = remember{
//                it.arguments?.getParcelable<Pokemon>("pokemon")
//            }

            pokemonJson?.let {
                DetailsScreen(Pokemon.fromJson(pokemonJson), navController)
            }

        //?: run {  }
        }
    }
}