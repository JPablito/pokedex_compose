package com.example.presentation.navigation

sealed interface NavigationRoutes {

    val route: String

    object SplashScreenRoute: NavigationRoutes {
        override val route: String = "splash_screen"
    }

    object ListPokemonScreenRoute: NavigationRoutes {
        override val route: String = "all_pokemon"
    }

}