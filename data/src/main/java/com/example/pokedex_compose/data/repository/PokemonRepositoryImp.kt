package com.example.pokedex_compose.data.repository

import com.example.pokedex_compose.data.remote.PokemonService
import com.example.pokedex_compose.domain.model.Pokemon
import com.example.pokedex_compose.domain.model.PokemonList
import com.example.pokedex_compose.domain.repository.PokemonRepository

class PokemonRepositoryImp(
    private val repository: PokemonService
    ) : PokemonRepository {

    override suspend fun getDetailsPokemon(name: String): Pokemon {
        return repository.detailsPokemon(name)
    }

    override suspend fun getAllPokemon(limit: Int): PokemonList {
        return repository.listAllPokemon(limit)
    }

}