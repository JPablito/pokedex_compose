package com.example.pokedex_compose.data.remote

import com.example.pokedex_compose.domain.model.Pokemon
import com.example.pokedex_compose.domain.model.PokemonList
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonService {

    @GET("pokemon/{pokemon}")
    suspend fun detailsPokemon(@Path("pokemon") pokemonName: String): Pokemon

    @GET("pokemon")
    suspend fun listAllPokemon(@Query("limit") limit: Int): PokemonList

}