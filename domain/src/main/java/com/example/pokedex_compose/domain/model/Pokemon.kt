package com.example.pokedex_compose.domain.model

import android.net.Uri
import android.os.Parcelable
import com.google.gson.Gson
import kotlinx.parcelize.Parcelize

@Parcelize
data class Pokemon(
    val name: String,
    val abilities: List<PokemonAbility>,
    val stats: List<Stats>,
    val sprites: Sprites
): Parcelable{

//    fun toJson(): String{
//        return Gson().toJson(this)
//    }

    companion object{

        fun fromJson(gson: String): Pokemon {
            return Gson().fromJson(gson, Pokemon::class.java)
        }

        fun Pokemon.toJson(): String{
            //return Gson().toJson(this)
            return Uri.encode(Gson().toJson(this))
        }

    }

}

@Parcelize
data class PokemonAbility(val ability: Ability): Parcelable

@Parcelize
data class Ability(val name: String): Parcelable

@Parcelize
data class Stats(val base_stat: Int, val stat: Stat): Parcelable

@Parcelize
data class Stat(val name: String): Parcelable

@Parcelize
data class Sprites(
    val front_default: String,
    val front_female: String,
    val front_shiny: String,
    val front_shiny_female: String
): Parcelable



