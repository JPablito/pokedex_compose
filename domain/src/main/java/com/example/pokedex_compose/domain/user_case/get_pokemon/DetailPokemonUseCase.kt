package com.example.pokedex_compose.domain.user_case.get_pokemon

import com.example.pokedex_compose.domain.model.Pokemon
import com.example.pokedex_compose.domain.repository.PokemonRepository

class DetailPokemonUseCase(
    private val repository: PokemonRepository
) {

    suspend fun execute(name: String): Pokemon {
        name.let { return repository.getDetailsPokemon(name) }
    }

}