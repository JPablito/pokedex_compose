package com.example.pokedex_compose.domain.repository

import com.example.pokedex_compose.domain.model.Pokemon
import com.example.pokedex_compose.domain.model.PokemonList


interface PokemonRepository {

    suspend fun getDetailsPokemon(name: String): Pokemon
    suspend fun getAllPokemon(limit: Int): PokemonList

}