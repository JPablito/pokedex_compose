package com.example.pokedex_compose.domain.model

data class PokemonList(
    val results: List<Pokemon>
)
