package com.example.pokedex_compose.domain.user_case.get_pokemons

import com.example.pokedex_compose.domain.model.PokemonList
import com.example.pokedex_compose.domain.repository.PokemonRepository


class ListAllPokemonUseCase(
    private val repository: PokemonRepository
) {

    suspend fun execute(limit: Int): PokemonList {
        limit.let { return repository.getAllPokemon(limit) }
    }

}