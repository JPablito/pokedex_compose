package com.example.pokedex_compose

import android.app.Application
import com.example.pokedex_compose.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin

class AppModule : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@AppModule)
            androidLogger()
            modules(
                netModule,
                detailPokemonUseCase,
                listAllPokemonUseCase,
                pokemonRepositoryImp,
                listAllPokemonViewModel
            )
        }

    }
}