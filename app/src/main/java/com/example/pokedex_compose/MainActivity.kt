package com.example.pokedex_compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.presentation.theme.Pokedex_composeTheme
import com.example.presentation.navigation.NavigationGraph

class MainActivity() : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            Pokedex_composeTheme {
                NavigationGraph()
            }
        }
    }

}


