package com.example.pokedex_compose.di

import com.example.pokedex_compose.data.remote.PokemonService
import com.example.pokedex_compose.data.repository.PokemonRepositoryImp
import com.example.pokedex_compose.domain.user_case.get_pokemon.DetailPokemonUseCase
import com.example.pokedex_compose.domain.user_case.get_pokemons.ListAllPokemonUseCase
import com.example.presentation.viewmodel.ListAllPokemonViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val listAllPokemonViewModel = module {
    viewModel { ListAllPokemonViewModel(get(), get()) }
}

val pokemonRepositoryImp = module {
    single { PokemonRepositoryImp(get()) }
}

val detailPokemonUseCase = module {
    single {
        DetailPokemonUseCase(
            PokemonRepositoryImp(
                get()
            )
        )
    }
}

val listAllPokemonUseCase = module {
    single {
        ListAllPokemonUseCase(
            PokemonRepositoryImp(
                get()
            )
        )
    }
}

val netModule = module {
    single { getInstance().create(PokemonService::class.java) }
}

fun getInstance(): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://pokeapi.co/api/v2/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}
